FROM amazonlinux:latest

RUN yum install -y httpd
COPY start.sh /start.sh
COPY index.html /var/www/html/index.html
COPY words.js /var/www/html/words.js
RUN chmod 644 /var/www/html/words.js
COPY canary.jpg /var/www/html/canary.jpg
COPY canary2.jpg /var/www/html/canary2.jpg
RUN chmod +x /start.sh
EXPOSE 80
CMD ["/start.sh"]
